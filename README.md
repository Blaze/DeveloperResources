# DeveloperResources

This repository contains data to help developers understand the Blaze protocol and packets.

* [**Blaze packet dump (dumped with BlazeHubHelper hook - check Blaze.LogLevel in IDA)**](blaze_packet_dump.txt)
* [Blaze component list (not completed yet)](COMPONENTS.MD)
