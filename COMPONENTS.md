|Component|Command|Component ID|Command ID|Type|
|---|---|---|---|---|
|RedirectorComponent|getServerInstance|0x0005|0x0001|
|AuthenticationComponent|login|0x0001|0x0028|
|AuthenticationComponent|loginPersona|0x0001|0x006E|
|UserSessions|UserAdded|0x7802|0x0002|ASYNC|
|UserSessions|UserUpdated|0x7802|0x0005|ASYNC|
|UserSessions|updateNetworkInfo|0x7802|0x0014|
|UtilComponent|postAuth|0x0009|0x0008|
|UtilComponent|getTelemetryServer|0x0009|0x0005|
|UtilComponent|ping|0x0009|0x0002|
|GameManager|NotifyGameStateChange|0x0004|0x0064|ASYNC|
|GameManager|NotifyGameSetup|0x0004|0x0014|ASYNC|
|GameManager|finalizeGameCreation|0x0004|0x000F|
|GameManager|advanceGameState|0x0004|0x0003|
